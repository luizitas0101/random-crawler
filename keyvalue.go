package main

type KeyValue struct {
	key string
	value string
}

func (kv KeyValue) ToQueryString() string {
	return kv.key + "=" + kv.value
}

func (kv KeyValue) ToHeader() string {
	return kv.key + ":" + kv.value
}
